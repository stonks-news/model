from sentence_transformers import SentenceTransformer
import pandas as pd
import os
import datetime
from typing import Tuple
import yfinance as yf
import torch
import numpy as np
import datetime

sentence_embedder = SentenceTransformer('bert-base-nli-mean-tokens')
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
symbols = sorted(['BAC', 'C', 'WFC', 'AAPL', 'CSCO', 'GOOGL', 'MSFT', 'INTC',
           'AMD', 'CCL', 'TRIP', 'T', 'AMZN', 'GE', 'BA', 'F', 'TM',
           'TSLA', 'GM', 'CHK', 'XOM', 'OXY', 'AAL', 'DAL', 'KO', 'BMRA',
           'JNJ', 'DIS', 'NFLX', 'WMT', 'ADDYY', 'KHC', 'NSRGF'])


def load_data(symbols: list, start: str, end: str):
  return yf.download(' '.join(symbols), start=start, end=end)


def create_features(start, end, label_percentage_jump=0.01):
  """
  Transform YFinance multiindex pandas dataset into market features and labels

  :param data - yfinance multiindex
  :param label_percentage_jump - % of price jump treated as label (ex.: >1% -> +1; <-1% -> -1; else 0)

  :return features dataframe (datetime index) |days|x|4|x|symbols|
  :return labels   dataframe (datetime index) |days|x|symbols|
  """
  _start = datetime.datetime.strptime(start, '%Y-%m-%d')
  _start = _start - datetime.timedelta(days=14)
  data = load_data(symbols, _start.strftime('%Y-%m-%d'), end)

  close = data[:]['Close']
  open = data[:]['Open']
  high = data[:]['High']
  low = data[:]['Low']

  # Labels
  labels = (open - close) / open
  labels = labels.where(labels < label_percentage_jump, 1)
  labels = labels.where(labels > -label_percentage_jump, -1)
  labels = labels.where((labels == 1.0) | (labels == -1.0), 0)

  postmarket_jump = (open - close.shift(1)) / close.shift(1)

  # Week jump, 6 first dates will be missed
  midprice = (close + open) / 2
  week_jump = (midprice.diff(periods=6) / midprice.shift(6)).shift(1)  # I will learn it next day

  # Rogers-Satchells (rs) volatility, 4 rows will be missing
  N = 5
  ln_hc = np.log(high / close)
  ln_ho = np.log(high / open)
  ln_lc = np.log(low / close)
  ln_lo = np.log(low / open)
  logs_sum = ln_hc * ln_ho + ln_lc * ln_lo
  rs = np.sqrt(logs_sum.rolling(N).sum() / N).shift(1)  # I will learn it next day

  # Yang and Zhang:  Garman and Klass + ROgers-Satchells + : (yz)

  k = 0.34 / (1.34 + (N + 1.0) / (N - 1.0))  # not implemented

  # 4 missing fields, Yang and Zhang + Garman and Klass
  ln_oc_p = np.log(open / close.shift(1))
  ln_hl = np.log(high / low)
  ln_co = np.log(close / open)
  logs_sum = ln_oc_p ** 2 + 0.5 * ln_hl ** 2 - (2 * np.log(2) - 1) * ln_co ** 2
  yzgk = np.sqrt(logs_sum.rolling(N).sum() / N).shift(1)

  features = pd.concat([postmarket_jump, week_jump, rs, yzgk],
                       keys=['postmarket-jump', 'week-jump', 'rs-vol', 'yzgk-vol'], axis=0)
  features.reset_index(level=0, inplace=True)
  features.drop('level_0', axis=1, inplace=True)

  # Remove first two weeks
  features = features.iloc[10:]
  labels = labels.iloc[10:]

  labels.index = labels.index.tz_localize(tz='UTC')
  features.index = features.index.tz_localize(tz='UTC')
  # labels.loc['2017-12-07'].values -> (33,)
  # features['2017-12-07'].values -> (4, 33)

  nans = []
  for row in features.isna().itertuples():
    # print(row)
    if row.AAL:
      nans.append(row.Index)

  features = features.drop(nans, errors='ignore')
  labels = labels.drop(nans, errors='ignore')
  return features, labels


class MarketDataset(torch.utils.data.Dataset):
  def __init__(self, features, labels):
    self.features = features
    self.labels = labels
    super(torch.utils.data.Dataset, self).__init__()

  def __len__(self):
    return len(self.labels)

  def __getitem__(self, idx):
    ts = self.labels.index[idx]
    market = torch.tensor(self.features.loc[ts].values, dtype=torch.float, device=device)
    labels = torch.tensor(self.labels.loc[ts].values, dtype=torch.long, device=device)
    return ts, market, labels + 1


def load_sentences(index_file: str, drop_duplicates=True, drop_ts_duplicates=False, header='infer', date_format=None):
  '''
  Common loader for all datasets with duplicates removal
  '''
  names = ['date', 'content'] if header is None else None
  df = pd.read_csv(index_file, header=header, names=names)
  df.dropna()
  time_index = pd.DatetimeIndex(pd.to_datetime(df.date, utc=True, format=date_format))
  df.index = time_index
  df.sort_index(inplace=True)
  if drop_duplicates:
    df.drop_duplicates(subset='content', inplace=True)
  if drop_ts_duplicates:
    df = df[~df.index.duplicated(keep='first')]
  df = df.loc[df.content.apply(type) == str]
  return df


def embed_sentence_to_parquet(news_file, drop_duplicates=False, drop_ts_duplicates=False, header='infer',
                              date_format=None):
  '''
  Method transforms news articles dataset into precomputed dataset of article embeddings.
  Applied tool is `SentenceTransformer`, which is pretrained on sentences (not articles)

  Function works on files with [`date`, `content`] structure
  '''

  df = load_sentences(news_file, drop_duplicates=drop_duplicates, drop_ts_duplicates=drop_ts_duplicates, header=header)
  sentences = df.content.values
  sent_embeddings = sentence_embedder.encode(sentences, show_progress_bar=True)
  df.content = sent_embeddings

  df.to_parquet(f'{news_file}-embeddings.parquet')


class DayNewsDataset(MarketDataset):
  def __init__(self, sentence_embeddings_file: str, features, labels, intraday: bool):
    '''
    Dataset combines `Market features & labels` with `News embeddings`

    Access via int 0, 1, 2...
    Return Tensor[Embeddings], Tensor[MFeatures], Tensor[Labels]
    '''
    super().__init__(features, labels)
    self.time_index, self.sentence_embeds = self._load_embeds(sentence_embeddings_file)
    self.intraday = intraday
    self.market_open = datetime.timedelta(hours=9, minutes=31)
    self.market_close = datetime.timedelta(hours=16)
    self._remove_empty_days()

  def _remove_empty_days(self):
    '''
    Remove days without news
    '''
    keys = list(self.labels.index)
    days_to_remove = []
    for idx, ts in enumerate(keys):
      mask = self._get_mask(ts)
      if not np.any(mask):
        print(f'Remove {ts}')
        days_to_remove.append(ts)

    self.labels.drop(days_to_remove, inplace=True, errors='ignore')
    self.features.drop(days_to_remove, inplace=True, errors='ignore')

  def _load_embeds(self, embedding_file: str):
    df = pd.read_parquet(embedding_file)
    time_index = pd.DatetimeIndex(pd.to_datetime(df.date, utc=True))
    df.index = time_index
    embeds = np.stack(df.content)
    return time_index, torch.tensor(embeds, dtype=torch.float)

  def _get_mask(self, ts):
    if self.intraday:
      # before = ts + self.market_open
      before = ts - datetime.timedelta(days=1) + self.market_close  # news from prev day close
      until = ts + self.market_close
    else:
      before = ts
      until = ts + datetime.timedelta(days=1)

    return (self.time_index >= before) & (self.time_index < until)

  def __getitem__(self, idx) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
    ts, market_features, labels = super().__getitem__(idx)
    idxs = self._get_mask(ts)
    embds = self.sentence_embeds[idxs]
    mask = torch.isnan(market_features[0])
    return (
    embds, market_features, labels, ~mask)  # Return also mask which says whether market feature of asset i-th is nan


def load_daynews_dataset(news_file, start_date, end_date, intraday: bool, drop_duplicates=False,
                         drop_ts_duplicates=False, header='infer', date_format=None) -> DayNewsDataset:
  '''
  Whole pipeline of loading dataset in one function

  1) Method automatically loads market features and creates market dataset
  * Market dataset takes care of rolling features: it preloads one week of data in order to compute first values
  * `Week jump` feature requires values of previous week to be computed

  2) Method loads sentence embeddings
  * Checks whether those are already precomputed, loads them if it is so
  * If there are no precomputed embeddings, initializes them and stores locally

  :param news_file:  ex.: 'american-news-1.csv'
  :param start_date: ex.: '2017-12-07'
  :param end_date:   ex.: '2018-06-01'
  :param intraday: whether news do have intraday timestamp (eg. 22-07-19 14:00:30) or just day only (22-07-19), important for DayNewsDataset to sample data

  :param drop_duplicates: if embeddings are not precomputed, whether to remove duplicates (`content` column)
  :param drop_ts_duplicates: if embeddings are not precomputed, whether to remove duplicates by time index (`date` column)
  :param header: whether .csv file contains header row or not (set None if not)
  '''
  features, labels = create_features(start_date, end_date)

  embed_file = f'{news_file}-embeddings.parquet'
  if not os.path.exists(embed_file):
    print('Precomputed embeddings not found -> create')
    embed_sentence_to_parquet(news_file, drop_duplicates=drop_duplicates, drop_ts_duplicates=drop_ts_duplicates,
                              header=header, date_format=date_format)
  else:
    print('Precomputed embeddings loaded')

  return DayNewsDataset(embed_file, features, labels, intraday)

def iter_dnd(dnd: DayNewsDataset):
  for sent_emb, features, labels, mask in dnd:
    sh = '<No news>' if sent_emb is None else sent_emb.shape
    print(sh, features.shape, labels.shape, mask.shape)
