import torch
from torch import nn

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class OneCompanyBranch(nn.Module):
  def __init__(self, dropout_rate, n_financial_features, context_emb_dim, n_classes, use_one_hot, n_companies):
    '''
    context_embeddings: [b, context_emb_dim]
    financial_features: [b, n_features]
    '''
    # input: context embedding + 4 features
    super(OneCompanyBranch, self).__init__()
    self.dropout_rate = dropout_rate
    if use_one_hot:
      self.fc1 = nn.Linear(context_emb_dim + n_financial_features + n_companies, context_emb_dim // 4)
    else:
      self.fc1 = nn.Linear(context_emb_dim + n_financial_features, context_emb_dim // 4)
    # self.fc2 = nn.Linear(context_emb_dim//2, context_emb_dim//4)
    self.fc3 = nn.Linear(context_emb_dim // 4, n_classes)

  def forward(self, context_embedding, financial_features):
    company_embedding = torch.cat([context_embedding, financial_features], dim=1)
    emb1 = nn.Dropout(self.dropout_rate)(nn.LeakyReLU()(self.fc1(company_embedding)))
    # emb2 = nn.Dropout(self.dropout_rate)(nn.LeakyReLU()(self.fc2(emb1)))
    return self.fc3(emb1)


class FinancePredictor(nn.Module):
  def __init__(self, branch_dropout_rate, context_dropout_rate,
               embedding_dim, context_hid_dim, context_emb_dim,
               n_financial_features, n_companies, n_classes,
               use_one_hot=False):
    super(FinancePredictor, self).__init__()
    self.n_financial_features = n_financial_features
    self.n_companies = n_companies
    self.n_classes = n_classes
    self.branch_dropout_rate = branch_dropout_rate
    self.context_dropout_rate = context_dropout_rate
    self.use_one_hot = use_one_hot  # if add one hot vector to identify company
    self.local_lstm = nn.LSTM(embedding_dim, context_hid_dim, bidirectional=False, batch_first=True)
    self.context_fc1 = nn.Linear(context_hid_dim, context_emb_dim)
    # self.context_fc2 = nn.Linear(context_hid_dim, context_emb_dim)
    self.company_layer = OneCompanyBranch(self.branch_dropout_rate, n_financial_features, context_emb_dim, n_classes,
                                          use_one_hot, n_companies)

  def forward(self, news_embeddings, financial_features):
    '''
    local_news: [b, n_articles, emb_size]
    financial_features: [b, features, n_companies]
    '''
    assert financial_features.shape[1] == self.n_financial_features
    assert financial_features.shape[2] == self.n_companies
    _, (news_repr, _) = self.local_lstm(news_embeddings)
    # news_repr = torch.cat([news_repr[0], news_repr[1]], dim=1)
    news_repr = news_repr[0]

    context_emb = nn.Dropout(self.context_dropout_rate)(nn.LeakyReLU()(self.context_fc1(news_repr)))
    # context_emb = nn.Dropout(self.context_dropout_rate)(nn.LeakyReLU()(self.context_fc2(context_emb))) # shape: [b, context_emb_dim]
    per_company_features = torch.transpose(financial_features, 1,
                                           2)  # [b, n_features, n_companies] -> [b, n_companies, n_features]
    predictions = []

    for i in range(self.n_companies):
      company_features = per_company_features[:, i, :]  # [b, n_features]
      if self.use_one_hot:
        one_hot = torch.zeros((financial_features.shape[0], self.n_companies))
        one_hot[:, i] = 1
        one_hot = one_hot.to(device)
        company_features = torch.cat([company_features, one_hot], dim=1)
      predictions.append(self.company_layer(context_emb, company_features))
    return predictions

def all_classes_acc(outputs, targets):
  def per_company_acc(output, target):
    # [b, 3], [b]
    predicted = torch.argmax(output, dim=1)
    acc = torch.sum(predicted==target, dtype=float)/targets.shape[0]
    # print('predicted: ', predicted, target, acc.item())
    return acc.item()
  accuracies = [per_company_acc(outputs[i], targets[:, i]) for i in range(targets.shape[1])]
  # print(accuracies)
  return accuracies


def create_default_model(symbols):
  EMBEDDING_DIM = 768
  CONTEXT_HID_DIM = 64
  CONTEXT_EMB_DIM = 32
  N_FEATURES = 4  # financial features
  USE_ONE_HOT = True
  N_COMPANIES = len(symbols)
  N_CLASSES = 3  # [1, 0 , -1]
  BRANCH_DROPUT_RATE = 0.3
  CONTEXT_DROPOUT_RATE = 0.3
  model = FinancePredictor(BRANCH_DROPUT_RATE, CONTEXT_DROPOUT_RATE, EMBEDDING_DIM,
                           CONTEXT_HID_DIM, CONTEXT_EMB_DIM, N_FEATURES,
                           N_COMPANIES, N_CLASSES, USE_ONE_HOT)
  model = model.to(device)
  return model

